import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-finished-test-list',
  templateUrl: './finished-test-list.component.html',
  styleUrls: ['./finished-test-list.component.scss']
})
export class FinishedTestListComponent implements OnInit {
  @Input() tests;
  constructor() { }

  ngOnInit() {
  }

}
