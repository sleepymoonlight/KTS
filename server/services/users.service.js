var config = require('../config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, {native_parser: true});
db.bind('users');

var service = {};

service.authenticate = authenticate;
service.create = create;
service.profile = profile;

module.exports = service;

function authenticate(name, password) {
  var deferred = Q.defer();

  db.users.findOne({name: name}, function (err, user) {
    if (err) deferred.reject(err.name + ': ' + err.message);

    if (user && bcrypt.compareSync(password, user.hash)) {
      deferred.resolve({
        _id: user._id,
        name: user.name,
        activeTests: user.activeTests,
        finishedTests: user.finishedTests,
        token: jwt.sign({sub: user._id}, config.secret)
      });
    } else {
      deferred.resolve();
    }
  });

  return deferred.promise;
}

function profile(name) {
  var deferred = Q.defer();
  db.users.findOne({name: name}, function (err, user) {
    if (err) deferred.reject(err.name + ': ' + err.message);
    if (user) {
      deferred.resolve({
        firstName: user.firstName,
        lastName: user.lastName

      });
    } else {
      deferred.resolve();
    }
  });

  return deferred.promise;
}

function create(userParam) {
  var deferred = Q.defer();
  db.users.findOne(
    {name: userParam.name},
    function (err, user) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      if (user) {
        deferred.reject('This username "' + userParam.name + '" is already taken');
      } else {
        createUser();
      }
    });

  function createUser() {
    var user = _.omit(userParam, 'password');
    user.hash = bcrypt.hashSync(userParam.password, 10);
    db.users.insert(
      user,
      function (err, doc) {
        if (err) deferred.reject(err.name + ': ' + err.message);
        deferred.resolve({msg: 'OK'});
      });
  }

  return deferred.promise;
}
