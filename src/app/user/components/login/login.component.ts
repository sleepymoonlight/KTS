import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../../login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public userCredentials: any = {
    email: '',
    password: '',
    isPaswordVisible: false
  };

  constructor(private loginService: LoginService, private router: Router) {
  }

  ngOnInit() {
  }

  private onSingIn(): void {
    this.loginService.login(this.userCredentials.email, this.userCredentials.password)
      .subscribe(() => {
        this.router.navigateByUrl('home');
      });
  }
}
