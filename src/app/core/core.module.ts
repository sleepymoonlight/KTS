import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  HeaderComponent,
  MenuComponent,
  UserInfoComponent,
  FooterComponent
} from './components';
import {RouterModule} from '@angular/router';
import {NotFoundComponent} from './components/not-found/not-found.component';
import {AuthService} from './auth.service';
import { SinginUpComponent } from './components/singin-up/singin-up.component';
import { ModalWindowComponent } from './components/modal-window/modal-window.component';


@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    HeaderComponent,
    MenuComponent,
    UserInfoComponent,
    FooterComponent,
    NotFoundComponent,
    SinginUpComponent,
    ModalWindowComponent
  ],
  providers: [AuthService],
  exports: [HeaderComponent, FooterComponent, ModalWindowComponent]
})
export class CoreModule {
}
