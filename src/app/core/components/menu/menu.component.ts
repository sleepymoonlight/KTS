import {Component, HostListener, OnInit} from '@angular/core';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})

export class MenuComponent implements OnInit {
  public isMenuVisible = false;

  constructor() {
  }

  ngOnInit() {
  }

  showMenu(): void {
    this.isMenuVisible = !this.isMenuVisible;
  }

  @HostListener('window:resize', ['$event'])
  onResize(event?: any) {
    this.isMenuVisible = false;
  }
}
