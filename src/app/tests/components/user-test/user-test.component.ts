import {Component, OnInit} from '@angular/core';
import {TestsService} from '../../tests.service';

@Component({
  selector: 'app-user-test',
  templateUrl: './user-test.component.html',
  styleUrls: ['./user-test.component.scss']
})
export class UserTestComponent implements OnInit {
  public isContentVisible = true;
  public activeTests;
  public passedTests;


  constructor(private testsService: TestsService) {
  }

  ngOnInit() {
    this.testsService.getTests()
      .subscribe((data: any) => {
        this.activeTests = data.tests;
      });
    // this.testsService.getPassedTests()
    //   .subscribe((data: any) => {
    //     this.passedTests = data.tests;
    //     console.log(this.passedTests);
    //   });
  }

  showContent(): void {
    this.isContentVisible = !this.isContentVisible;
  }
}
