import {Injectable} from '@angular/core';
import {AuthService} from '../core/auth.service';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {LoginService} from './login.service';


@Injectable()
export class RegistrationService {

  constructor(private http: HttpClient, private auth: AuthService,
              private login: LoginService) {
  }

  registration(email: string, password: string) {
    const body = {
      name: email,
      password: password
    };
    return this.http.post('http://localhost:4000/users/register', body)
      .pipe(map((res: { token: string }) => {
        const resp = res;
        console.log(res);
        this.login.login(email, password).subscribe((d) => {
          this.auth.setToken(d.token);

        });

        return resp;
      }));
  }

}
