import {Component, Input, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-test-list',
  templateUrl: './test-list.component.html',
  styleUrls: ['./test-list.component.scss']
})
export class TestListComponent implements OnInit {
  @Input() tests;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  public onStart(testId: number) {
    this.router.navigateByUrl('my-tests/open/' + testId);
  }
}

