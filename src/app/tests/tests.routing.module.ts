import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {UserTestComponent} from './components/user-test/user-test.component';
import {TestPageComponent} from './components/test-page/test-page.component';
import {TestsStatisticComponent} from './components/tests-statistic/tests-statistic.component';

const testsRoutes: Routes = <Routes>[
  {
    path: 'my-tests', children: [
      {path: 'open/:id', component: TestPageComponent},
      {path: 'statistic/:id', component: TestsStatisticComponent},
      {path: '', component: UserTestComponent, pathMatch: 'full'}
    ]
  }];

@NgModule({
  imports: [
    RouterModule.forChild(testsRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class TestsRoutingModule {

}
