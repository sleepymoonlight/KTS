var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
var expressJwt = require('express-jwt');
var config = require('./config.json');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// // use JWT auth to secure the api
// app.use(expressJwt({
//     secret: config.secret,
//     getToken: function fromHeaderOrQuerystring(req) {
//         if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
//             return req.headers.authorization.split(' ')[1];
//         } else if (req.query && req.query.token) {
//             return req.query.token;
//         }
//     }
// }).unless(
//     {
//         path: [
//             '/users/authenticate',
//             '/users/register',
//
//           // TODO auth
//           '/tests/all'
//         ]
//     }));

// routes
app.use('/users', require('./controllers/users.controller'));
app.use('/tests', require('./controllers/tests.controller'));


// start server
var port = process.env.NODE_ENV === 'production' ? 80 : 4000;
var server = app.listen(port, function () {
    console.log('Server listening on port ' + port);
});
