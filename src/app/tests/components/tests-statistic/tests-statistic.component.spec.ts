import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestsStatisticComponent } from './tests-statistic.component';

describe('TestsStatisticComponent', () => {
  let component: TestsStatisticComponent;
  let fixture: ComponentFixture<TestsStatisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestsStatisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestsStatisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
