var config = require('../config.json');
var _ = require('lodash');
var Q = require('q');
var mongo = require('mongoskin');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var db = mongo.db(config.connectionString, {native_parser: true});
db.bind('tests');
db.bind('users');
db.bind('answers');

var service = {};

service.add = add;
service.edit = edit;
service.getById = getById;
service.getAlltests = getAlltests;
service.getAlltestsFinished = getAlltestsFinished;
service.delete = deletetests;
service.search = search;
service.check = check;

module.exports = service;

function add(tests) {
  var deferred = Q.defer();
  db.tests.insert(tests,
    function (err) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      deferred.resolve();
    });
  return deferred.promise;
}

function edit(tests) {
  console.log(tests);
  var deferred = Q.defer();
  db.tests.update(
    {id: tests.id},
    {
      $addToSet: {
        id: tests.id,
        date: tests.date,
        name: tests.name,
// TODO model for update
      }


    }, function (err) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      deferred.resolve();
    });

  return deferred.promise;
}

function getById(testsId) {
  var deferred = Q.defer();
  db.tests.findOne({id: Number(testsId)}, function (err, tests) {
    if (err) deferred.reject(err.name + ': ' + err.message);
    if (tests) {
      deferred.resolve(tests.questions);
    } else {
      deferred.resolve();
    }
  });

  return deferred.promise;
}

function getAlltests(name, page) {
  var deferred = Q.defer();
  db.users.findOne(
    {name: name},
    function (err, user) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      if (!user) {
        deferred.reject('error');
      } else {
        getTests(user.testsInProgress);
      }
    });

  function getTests(ids) {
    var perPage = 5;
    var _page = Number(page) || 1;
    var bulk = db.tests.findItems({id: {"$in": ids}}, {}, {
      skip: (perPage * _page) - perPage,
      limit: perPage
    }, function (err, tests) {
      if (err) deferred.reject(err.name + ': ' + err.message);
      if (tests) {
        db.tests.count(function (error, count) {
          if (error) deferred.reject(error.name + ': ' + error.message);
          deferred.resolve({
            tests: tests,
            current: page,
            pages: Math.ceil(count / perPage)
          });
        })
      } else {

      }
    });

  }

  return deferred.promise;
}

function getAlltestsFinished(id, page) {
  var perPage = 5;
  var _page = Number(page) || 1;
  var deferred = Q.defer();
  var bulk = db.tests.findItems({id: ids}, {}, {
    skip: (perPage * _page) - perPage,
    limit: perPage
  }, function (err, tests) {
    if (err) deferred.reject(err.name + ': ' + err.message);
    if (tests) {
      db.tests.count(function (error, count) {
        if (error) deferred.reject(error.name + ': ' + error.message);
        deferred.resolve({
          tests: tests,
          current: page,
          pages: Math.ceil(count / perPage)
        });
      })

    } else {
      deferred.resolve();
    }
  });
  return deferred.promise;
}

function search(subStr) {
  var deferred = Q.defer();
  var reg = ".*" + subStr + ".*";
  db.tests.findItems({"name": {$regex: reg}}, function (err, tests) {
    if (err) deferred.reject(err.name + ': ' + err.message);
    if (tests) {
      deferred.resolve(tests);
    } else {
      deferred.resolve();
    }
  })
  return deferred.promise;
}

function deletetests(id) {
  var deferred = Q.defer();
  db.tests.removeOne({id: id},
    function (err) {
      if (err) deferred.reject(err.name + ': ' + err.message);

      deferred.resolve();
    });

  return deferred.promise;
}

function check(answers) {
  var deferred = Q.defer();
  db.answers.findOne({testID: Number(answers.id)}, function (err, _answers) {
    if (err) deferred.reject(err.name + ': ' + err.message);
    if (_answers) {
      var correct = compare(answers.userAnswers, _answers.correctAnswers);
      deferred.resolve({
        score: (correct / answers.userAnswers.length * 100).toFixed(2),
        correct: correct + ' / ' + answers.userAnswers.length
      });
    } else {
      deferred.reject('no answers');
    }
  });
  return deferred.promise;
}

function compare(arr1, arr2) {
  var correctAnswers = 0;

  for (var i = 0; i < arr1.length; i++) {
    if (arr1[i] == arr2[i]) {
      correctAnswers++;
    }
  }
  return correctAnswers;
}

