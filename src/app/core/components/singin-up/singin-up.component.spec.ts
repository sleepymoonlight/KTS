import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SinginUpComponent } from './singin-up.component';

describe('SinginUpComponent', () => {
  let component: SinginUpComponent;
  let fixture: ComponentFixture<SinginUpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SinginUpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SinginUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
