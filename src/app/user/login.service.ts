import {Injectable} from '@angular/core';
import {AuthService} from '../core/auth.service';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable()
export class LoginService {

  constructor(private http: HttpClient, private auth: AuthService) {
  }

  login(email: string, password: string) {
    const body = {
      name: email,
      password: password
    };
    return this.http.post('http://localhost:4000/users/authenticate', body)
      .pipe(map((res: { token: string, name: string }) => {
        const resp = res;
        console.log(res);
        this.auth.setToken(resp.token);
        localStorage.setItem('name', res.name);
        this.auth.isLogined.next(true);
        return resp;
      }));
  }

}
