var config = require('../config.json');
var express = require('express');
var router = express.Router();
var testsService = require('../services/tests.service.js');

// routes
router.post('/add', add);
router.post('/edit', edit);
router.get('/all', getAll);
router.get('/finished', getAllFinished);
router.get('/getById', getById);
router.get('/search', search);
router.post('/check', check);
router.delete('/delete', deletetests);


module.exports = router;

function check(req, res) {
  testsService.check(req.body)
    .then(function (result) {
      if (result) {
        console.log('result')
        console.log(result)
        res.send(result);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}

function add(req, res) {
  testsService.add(req.body)
    .then(function () {
      res.sendStatus(200);
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}

function edit(req, res) {
  testsService.edit(req.body)
    .then(function () {
      res.sendStatus(200);
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}

function getAll(req, res) {
  testsService.getAlltests(req.query.name, req.query.page)
    .then(function (tests) {
      if (tests) {
        res.send(tests);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}

function getAllFinished(req, res) {
  testsService.getAlltestsFinished(req.query.id, req.query.page)
    .then(function (tests) {
      if (tests) {
        res.send(tests);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}

function getById(req, res) {
  testsService.getById(req.query.id)
    .then(function (tests) {
      if (tests) {
        res.send(tests);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}

function search(req, res) {
  testsService.search(req.query.subStr)
    .then(function (tests) {
      if (tests) {
        res.send(tests);
      } else {
        res.sendStatus(404);
      }
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}

function deletetests(req, res) {
  testsService.delete(req.body.id)
    .then(function () {
      res.sendStatus(200);
    })
    .catch(function (err) {
      res.status(400).send(err);
    });
}
