import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Question} from './question.interface';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  @Input() question: Question;
  @Input() groopId: string;
  @Output() appOnValueChanged = new EventEmitter();

  public value = '';

  constructor() {
  }

  ngOnInit() {
  }

  public onChange(e: any): void {
    console.log(e);
    this.appOnValueChanged.emit(e.target.value);
  }

}
