import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {TestsService} from '../../tests.service';

@Component({
  selector: 'app-test-page',
  templateUrl: './test-page.component.html',
  styleUrls: ['./test-page.component.scss']
})
export class TestPageComponent implements OnInit {
  public questions;
  public answers;
  public isResultVisible = false;
  public testID;
  public result;

  constructor(private activatedRoute: ActivatedRoute,
              private testService: TestsService) {
  }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe(params => {
      this.getTestById(params['id']);
      this.testID = params['id'];
    });
  }

  public onValueChanged(value: string, index: number) {
    this.answers[index] = value;
    console.log(this.answers);
  }

  public getTestById(testId: number) {
    this.testService.getTestById(testId)
      .subscribe(data => {
        this.questions = data;
        this.answers = new Array(this.questions.length).fill('');
      });
  }

  public onSubmit(): void {
    this.testService.check(this.testID, this.answers)
      .subscribe(data => {
        this.result = data;
        this.isResultVisible = true;

      });
  }

  public closeResult(): void {
    this.isResultVisible = false;
  }

}
