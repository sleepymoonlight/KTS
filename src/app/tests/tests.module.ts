import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TestListComponent} from './components/test-list/test-list.component';
import {UserTestComponent} from './components/user-test/user-test.component';
import {FinishedTestListComponent} from './components/finished-test-list/finished-test-list.component';
import {TestsService} from './tests.service';
import {HttpClientModule} from '@angular/common/http';
import {TestsStatisticComponent} from './components/tests-statistic/tests-statistic.component';
import {TestsRoutingModule} from './tests.routing.module';
import {TestPageComponent} from './components/test-page/test-page.component';
import {QuestionComponent} from './components/question/question.component';
import {FormsModule} from '@angular/forms';
import {CoreModule} from '../core/core.module';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    TestsRoutingModule,
    CoreModule
  ],
  declarations: [TestListComponent,
    UserTestComponent,
    FinishedTestListComponent,
    TestPageComponent,
    TestsStatisticComponent,
    QuestionComponent],
  providers: [TestsService]
})
export class TestsModule {
}
