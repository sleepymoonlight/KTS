import { Injectable } from '@angular/core';
import {BehaviorSubject} from 'rxjs';

@Injectable()
export class AuthService {

  isLogined = new BehaviorSubject(this.isLogedIn());
  setToken(token: string): void {
    localStorage.setItem('token', token);
  }

  getToken(): string {
    return localStorage.getItem('token') || '';
  }

  isLogedIn(): boolean {
    return !!localStorage.getItem('token');
  }

  logOut(): void {
    localStorage.clear();
  }
}
