import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {UserRoutingModule} from './user.routing.module';
import {FormsModule} from '@angular/forms';
import {LoginService} from './login.service';
import {RegistrationService} from './registration.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    UserRoutingModule
  ],
  declarations: [LoginComponent, RegistrationComponent],
  providers: [LoginService, RegistrationService]
})
export class UserModule {
}
