import {Component, HostListener, OnInit} from '@angular/core';
import {AuthService} from '../../auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
  public isLoginVisible = false;

  constructor(private auth: AuthService, private router: Router) {
  }

  ngOnInit() {
  }

  public logout(): void {
    localStorage.clear();
    this.auth.isLogined.next(false);
    this.router.navigateByUrl('/user/login');
  }

  @HostListener('window:resize', ['$event'])
  onSesize(event?: any) {
    this.isLoginVisible = false;
  }
}

