var config = require('../config.json');
var express = require('express');
var router = express.Router();
var userService = require('../services/users.service.js');

router.post('/authenticate', authenticate);
router.post('/register', register);
router.post('/profile', profile);

module.exports = router;

function authenticate(req, res) {
    userService.authenticate(req.body.name, req.body.password)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.status(401).send('Username or password is incorrect');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function register(req, res) {
    userService.create(req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
function profile(req, res) {
    userService.profile(req.body.username)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.status(500).send('Error occured');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}
