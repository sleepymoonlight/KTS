import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {LoginService} from '../../login.service';
import {RegistrationService} from '../../registration.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {
  public userCredentials: any = {
    email: '',
    password: '',
    isPaswordVisible: false
  };

  constructor(private registrationService: RegistrationService, private router: Router) {
  }

  ngOnInit() {
  }

  private onSingUp(): void {
    this.registrationService.registration(this.userCredentials.email, this.userCredentials.password)
      .subscribe(() => {
        this.router.navigateByUrl('home');
      });
  }
}
