import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';

@Injectable()
export class TestsService {
  constructor(private http: HttpClient) {
  }

  public getTests() {
    const params = new HttpParams().set('name', localStorage.getItem('name'));

    return this.http.get('http://localhost:4000/tests/all', {params});
  }

  // public getPassedTests() {
  //   return this.http.get('http://localhost:4000/tests/finished');
  // }

  public getTestById(testId) {
    const params = new HttpParams().set('id', testId);

    return this.http.get('http://localhost:4000/tests/getById', {params});
  }

  public check(id, userAnswers) {
    const body = {
      id,
      userAnswers
    };
    return this.http.post('http://localhost:4000/tests/check', body);
  }
}
